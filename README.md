# BJSON
### a json parser for BASH or any other shell

A (almost purely) POSIX-compliant shell tool to work with json. No subshells, no external commands except for `echo`. 

The only non-POSIX part is `read -N1` which does not have any native alternative in POSIX shells. DASH and SH seem to support it anyway (at least at the time of writing)

## NOTE! This project is still a WIP

Usage:

1. source it to your environment

    `. bjson.sh`

2. get a variable with JSON data

    `json=$(curl -s -kL -X GET 'https://devrant.com/api/devrant/rants/surprise?app=3')`

3. extract data to the environment

    `parseJson "${json}"`

    or

    `bjson_parse "${json}"`

4. extract values you need

    `BJMODE=find parseJson "rant.attached_image.url"`

    `BJMODE=find parseJson "rant.tags"`

    `BJMODE=find parseJson "rant.tags[0]"`

    or

    `bjson_find "rant.attached_image.url"` ## bash autocompletion partially works in path lookup. Don't push it, it's a WIP :) 
    
5. update extracted values

    `BJMODE=write parseJson "rants.tags[1]" "nonsense" string`

    or

    `bjson_write "rants.tags[1]" "nonsense" string`

6. compile extracted data back to JSON

    `BJMODE=compile parseJson`

    or

    `bjson_compile`

5. To cleanup your environment run

    `clearParsed`

    or

    `bjson_clear`

That's it. 


Calls/constructs used by this tool:

- echo
- eval
- set
- unset
- read
- case
- if
- elif
- while
- for
- break
- continue
 

