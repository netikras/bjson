#!/bin/bash

#title           :bjson.sh
#description     :POSIX-compliant pure shell library to work with JSON structures. 
#                :  No subshells, no external binaries.
#author          :Darius Juodokas (aka netikras)
#date            :2018-08-03
#version         :0.8
#url             : https://gitlab.com/netikras/bjson
#usage           :  . bjson.sh; 
#                :  parseJson "${json}"; 
#                :  BJMODE=find parseJson "company.employees[4].name"
#                :  BJMODE=write parseJson "company.employees[4].name" John string
#                :  BJMODE=compile parseJson
#notes           :Writes parsed values to global environment!
#                :
#                :  Still WIP
#===============================================================================


## {{{ Junk

#json=${1}
#json=$(curl -s -kL -X GET 'https://devrant.com/api/devrant/rants/surprise?app=3')

#read -r -d '' json <<EOJSON
#{"success":true,"rants":[{"id":1577705,"text":"Database- I'm not feeling good","score":162,"created_time":1532241817,"attached_image":{"url":"https:\/\/img.devrant.com\/devrant\/rant\/r_1577705_wTeho.jpg","width":800,"height":996},"num_comments":4,"tags":["joke\/meme","php","funny","mysql","table"],"vote_state":0,"edited":false,"rt":1,"rc":3,"user_id":1431040,"user_username":"amansetiarjp","user_score":580,"user_avatar":{"b":"d55161","i":"v-20_c-3_b-5_g-m_9-1_1-1_16-7_3-2_8-1_7-1_5-1_12-1_6-10_10-7_2-57_22-10_15-5_18-2_19-1_4-1.jpg"},"user_avatar_lg":{"b":"d55161","i":"v-20_c-1_b-5_g-m_9-1_1-1_16-7_3-2_8-1_7-1_5-1_12-1_6-10_10-7_2-57_22-10_15-5_18-2_19-1_4-1.png"}}],"settings":[],"set":"5b54fb3846957","wrw":113,"news":{"id":149,"type":"rant","headline":"New devRant feature!","body":"Avatar expressions are here!","footer":"Tap to learn more","height":100,"action":"rant-1563683"}}
#EOJSON

## DEBUG:
## export PS4="+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }" ; set -x

#cleanAll() {
#    while read sett ; do unset ${sett%=*} ; done <<< "$(set | egrep "^[A-Z]{3}_BASE_")"
#}

## }}}


## {{{ Settings

## This is entirely for internals. This value is used to form shell variable names 
## and it must not clash with any field names in the json. 
## Just keep it in mind and change if there is a need to.

VAR_DELIM="__"

## JSON spec allows KEY to be any string value. Since shell does only allow variable
## names made out of digits, letters and underscores, alien values in KEY will be 
## replaced with VAL_REPLC.

VAL_REPLC="________________"

## Each object and array can have members. They are all listed in respective MMB_BASE_* 
## variables. MMB_DELIM is used to separate those values from each other. It should not
## be a substring of any value of the JSON structure

MMB_DELIM=$'\x05'



## These are the core (BASE) variables. JSON structure will be projected onto them. 
## Change their names if they might clash with smth in your environment
KEY_BASE=${KEY_BASE:-KEY_BASE};
TYP_BASE=${TYP_BASE:-TYP_BASE};
MMB_BASE=${MMB_BASE:-MMB_BASE};
LEN_BASE=${LEN_BASE:-LEN_BASE};
UPP_BASE=${UPP_BASE:-UPP_BASE};
IDX_BASE=${IDX_BASE:-IDX_BASE};

## }}}


## {{{ Internal variables and functions

BJPATHS=;

isSet() {
    [ -n "${1}" ] && return 0 || return 1;
}

registerPath() {
    BJPATHS="${BJPATHS}${MMB_DELIM}${1}";
}

## }}}


## {{{ clearParsed()

clearParsed() {
    while read _BJPATH ; do 
        for _BJVAR in KEY_BASE TYP_BASE MMB_BASE LEN_BASE UPP_BASE IDX_BASE CUR_ITEM ; do
            eval "unset ${_BJVAR}${VAR_DELIM}${_BJPATH}" ; 
        done;

        ## newline below is intentional!
    done <<< "${BJPATHS//${MMB_DELIM}/
}"
    BJPATHS=;
   :; 
}

## }}}



## {{{ parseJson()

parseJson() {

    local str=${1};

#    if [ -z "${str}" ] ; then
#        read str;
#    fi;

    local MODE=${BJMODE:-parse}

    ## {{{ variables' declaration
    
    local TAB=$'\t'
    local CR=$'\r'
    local NL=$'\n'

    local KEY=;     ## KEY in key-value pair
    local VAL=;     ## VALUE in key-value pair
    local FIN=;     ## key-value duplet has been read

    local ESC=;     ## char is escaped
    local STR=;     ## inside string (either key or value)
    local LIT=1;    ## literal (null|true|false|number)

    local TOK="";   ## The complete token read (either string or literal)
    local END=;     ## End of token (second ", comma, closing block, etc.)
    local SKIP=;    ## Do not append this current char to TOKEN
    local NOVAL=1;  ## There's still no value present.. Is object/array empty?

    local UNICODE=;
    local UNI_CT=0;

    local PERIOD=;

    local CUR_PATH=;
    local UPP_PATH=;
    local NEW_BLOCK=;
    local END_BLOCK=;

    local _type=;
    local _p_type=;

    local CUR_ITEM=;

    ## }}}

    ## {{{ reading and interpreting chars of ${str} (${1})
    ## This is the only non-posix part -- read -N1
    while IFS="" read -N1 -r c ; do
        
        NEW_BLOCK=;
        END_BLOCK=;
        FIN=;
        END=;
        SKIP=;
        PERIOD=;

        _c=______________________________________________${c} ## for debug
        
        if isSet ${UNICODE} ; then
            ## {{{ Unicode parsing
            SKIP=1;
            if [ "xy" = "x${UNICODE}" ] ; then
                UNICODE="";
            fi;
            ## only digits!
            UNICODE="${UNICODE}${c}";
            UNI_CT=$((UNI_CT+1));

            if [[ ${UNI_CT} -eq 4 ]] ; then
                c="\\u${UNICODE}";
                UNI_CT=0;
                UNICODE=;
                SKIP=;
                :;
            fi;
            ## }}}
        else
            ## {{{ Determining char type and setting flags
            case "${c}" in
                "\\") 
                    LIT=;
                    if isSet ${ESC} ; then 
                        ESC=; 
                    else 
                        ESC=1;
                        SKIP=1;
                    fi; 
                    ;;
                "\"") 
                    LIT=;
                    c="\\\"";
                    if isSet ${STR} ; then
                        if isSet ${ESC} ; then 
                            ESC=;
                        else 
                            NOVAL=;
                            END=1;
                            SKIP=1;
                        fi;
                    else
                        SKIP=1;
                        STR=1; ## beginning of string
                    fi;
                    ;;
                "n")
                    if isSet ${ESC} && isSet ${STR} ; then
                        ESC=;
                        c=${NL};
                    fi;
                    ;;
                "r")
                    if isSet ${ESC} && isSet ${STR} ; then
                        ESC=;
                        c=${CR}
                    fi;
                    ;;
                "t")
                    if isSet ${ESC} && isSet ${STR} ; then
                        ESC=;
                        c=${TAB}
                    fi;
                    ;;
                "/")
                    if isSet ${ESC} && isSet ${STR} ; then
                        ESC=;
                    fi;
                    ;;
                "u")
                    if isSet ${ESC} && isSet ${STR} ; then
                        ESC=;
                        SKIP=1;
                        UNICODE=y;
                    fi;
                    ;;
                "[") 
                    if isSet ${STR} ; then 
                        :; 
                    else
                        SKIP=1;
                        NEW_BLOCK=array; 
                        LIT=;
                        c=;
                        :; 
                    fi;
                    ;;
                "{")
                    if isSet ${STR} ; then 
                        :; 
                    else 
                        SKIP=1;
                        NEW_BLOCK=object;
                        LIT=;
                        c=;


                        :; 
                    fi;
                    ;;
                "]")
                    if isSet ${STR} ; then 
                        :; 
                    else 
                        END=1;
                        SKIP=1;
                        #LIT=;
                        END_BLOCK=array;
                        c=;
                        :; 
                    fi;
                    ;;
                "}")
                    if isSet ${STR} ; then 
                        :; 
                    else 
                        END=1;
                        SKIP=1;
                        #LIT=;
                        END_BLOCK=object;
                        c=;
                        :; 
                    fi;
                    ;;
    
                    ## These only make sense in STRING tokens (key/value)
                " ")      if ! isSet ${STR} ; then continue; fi; ;;
                "")       if ! isSet ${STR} ; then continue; fi; ;;
                "${CR}")  if ! isSet ${STR} ; then continue; fi; ;;
                "${NL}")  if ! isSet ${STR} ; then continue; fi; ;;
                "${TAB}") if ! isSet ${STR} ; then continue; fi; ;;
    
                ",")
                    if isSet ${STR} ; then
                        :;
                    else
                        END=1;
                        SKIP=1;
                    fi;
                    ;;
                ".")
                    if isSet ${ESC} ; then
                        ESC=;
                    else
                        PERIOD=1;
                    fi;
                    ;;
                ":")
                    if isSet ${STR} ; then
                        :;
                    else
                        SKIP=1;
                    fi;
                    ;;
                \`|\$)
                    c="\\${c}";
                    ;;
                *)
                    ;;
            esac;
            ## }}}
        fi;


        case "${MODE}" in
            parse)

    
            eval "_type=\${${TYP_BASE}${VAR_DELIM}${CUR_PATH}}";
    
            if isSet ${SKIP} ; then
                :;
            else 
                if isSet ${STR} ; then
                    if isSet "${KEY}" || [ "${_type}" = "array" ] ; then
                        TOK="${TOK}${c}";
                    else
                        case "${c}" in 
                            [a-zA-Z_0-9]) TOK="${TOK}${c}";;
                            *) TOK="${TOK}${VAL_REPLC}";;
                        esac
                    fi;
                    NOVAL=;
                elif isSet ${LIT} ; then
                    TOK="${TOK}${c}";
                    NOVAL=;
                fi;
            fi;
    
            if isSet ${END} ; then ## one whole token has been read
                if isSet ${NOVAL} ; then
                    :;
                else
                    _TOK=________________________________________${TOK}___________________________ ## for debug
                    eval "_type=\${${TYP_BASE}${VAR_DELIM}${CUR_PATH}}"
                    eval "_p_type=\${${TYP_BASE}${VAR_DELIM}${UPP_PATH}}"
    
                    if [ "${_type}" = "array" ] ; then
                        ## {{{ Token read belongs to an array
                        eval "KEY=\${${LEN_BASE}${VAR_DELIM}${CUR_PATH}}"
                        
                        
                        VAL=${TOK}
                        FIN=1
                        registerPath "${CUR_PATH}${VAR_DELIM}${KEY}"
                        eval "${KEY_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=\"${TOK}\""
                        eval "${MMB_BASE}${VAR_DELIM}${CUR_PATH}=\"\${${MMB_BASE}${VAR_DELIM}${CUR_PATH}}${MMB_DELIM}\${${LEN_BASE}${VAR_DELIM}${CUR_PATH}}\""
                        eval "${LEN_BASE}${VAR_DELIM}${CUR_PATH}=\$((${LEN_BASE}${VAR_DELIM}${CUR_PATH}+1))"
                        if isSet ${LIT} ; then 
                            eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=literal" ; 
                            _type=literal;
                        elif isSet ${STR} ; then
                            eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=string" ; 
                            _type=string
                        fi;
                        :;
                        ## }}}
                    elif isSet ${KEY} ; then
                        ##{{{ Token read is linked to a KEY
                        VAL=${TOK}
                        if isSet ${LIT} ; then 
                            eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=literal" ; 
                            _type=literal;
                        elif isSet ${STR} ; then
                            eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=string" ; 
                            _type=string
                        else
                            echo "Something messed up at token: '${KEY}=${TOK}'" >&2
                            return;
                        fi;
    
                        eval "${KEY_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=\"${VAL}\"";
                        eval "${LEN_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=\$((${LEN_BASE}${VAR_DELIM}${CUR_PATH}+1))";
                        eval "${MMB_BASE}${VAR_DELIM}${CUR_PATH}=\"\${${MMB_BASE}${VAR_DELIM}${CUR_PATH}}${MMB_DELIM}${KEY}\"";
    
                        registerPath "${CUR_PATH}${VAR_DELIM}${KEY}"
                        
                        FIN=1;
                        ##}}}
                        :;
                    else
                        ##{{{ Token read IS a KEY
                        KEY=${TOK}
                        ##}}}
                        :;
                    fi;
                    TOK=
                fi;
    
                STR=;
                LIT=1;
                ESC=;
                NOVAL=1;
                
                TOK=;
            fi;
            
            if isSet ${NEW_BLOCK} ; then
                ## {{{ NEW_BLOCK
                if [ "${_type}" = "array" ] ; then 
                    eval "KEY=\${${LEN_BASE}${VAR_DELIM}${CUR_PATH}}";
                else 
                    :;
                fi;
                eval "${KEY_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}="
                eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=${NEW_BLOCK}"
                eval "${MMB_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}="
                eval "${LEN_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=0"
                eval "${UPP_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}=${CUR_PATH}"
                eval "${IDX_BASE}${VAR_DELIM}${CUR_PATH}${VAR_DELIM}${KEY}="
    
                eval "${MMB_BASE}${VAR_DELIM}${CUR_PATH}=\${${MMB_BASE}${VAR_DELIM}${CUR_PATH}}${MMB_DELIM}${KEY}"
                eval "${LEN_BASE}${VAR_DELIM}${CUR_PATH}=\$((${LEN_BASE}${VAR_DELIM}${CUR_PATH}+1))"
    
                eval "UPP_PATH=${CUR_PATH}"
                eval "CUR_PATH=${CUR_PATH}${VAR_DELIM}${KEY}"
                registerPath "${CUR_PATH}"
                KEY=;
                FIN=;
                ## }}}
            elif isSet ${END_BLOCK} ; then
                ##{{{ END_BLOCK
                eval "CUR_PATH=${UPP_PATH}"
                eval "UPP_PATH=\${${UPP_BASE}${VAR_DELIM}${CUR_PATH}}"
                ##}}}
            fi;

            if isSet ${FIN} ; then ## key-value pair has been read
                KEY=;
                VAL=;
            fi;
    
            START=;
            ;;
        compile)
            break;
            ;;
        find)
            
            if [ -z "${CUR_PATH}" ] ; then
                for _prepend in {1..2} ; do
                    CUR_PATH="${CUR_PATH}${VAR_DELIM}";
                done;
            fi;
            
            if isSet ${PERIOD} ; then
                CUR_PATH="${CUR_PATH}${VAR_DELIM}";
            elif [ "${NEW_BLOCK}" = "array" ] ; then
                CUR_PATH="${CUR_PATH}${VAR_DELIM}";
            elif isSet ${END_BLOCK} ; then
                :;
            else
                CUR_PATH="${CUR_PATH}${c}"
            fi;

            ;;
        write)
            
            if [ -z "${CUR_PATH}" ] ; then
                for _prepend in {1..2} ; do
                    CUR_PATH="${CUR_PATH}${VAR_DELIM}";
                done;
            fi;
            
            if isSet ${PERIOD} ; then
                UPP_PATH=${CUR_PATH};
                CUR_PATH="${CUR_PATH}${VAR_DELIM}";
                KEY=;
            elif [ "${NEW_BLOCK}" = "array" ] ; then
                UPP_PATH=${CUR_PATH};
                CUR_PATH="${CUR_PATH}${VAR_DELIM}";
                KEY=;
            elif isSet ${END_BLOCK} ; then
                :;
            else
                KEY="${KEY}${c}";
                CUR_PATH="${CUR_PATH}${c}";
            fi;
            ;;
    esac;
            
    done <<< "${str}";

    ## }}}

    ## {{{ POST-processing

    ## {{{ find by key
    if [ "${MODE}" = "find" ] ; then
        eval "_type=\${${TYP_BASE}${VAR_DELIM}${CUR_PATH}}";
        if [ "${_type}" = "array" -o "${_type}" = "object" ] ; then 
            eval "VAL=\${${MMB_BASE}${VAR_DELIM}${CUR_PATH}//${MMB_DELIM}/, }"
            VAL=${VAL/, /}
            :;
        else
            eval "VAL=\${${KEY_BASE}${VAR_DELIM}${CUR_PATH}}"
        fi;

        echo "${VAL}";
    ## }}}

    ## {{{ write new value
    elif [ "${MODE}" = "write" ] ; then
        
        eval "_p_type=\${${TYP_BASE}${VAR_DELIM}${UPP_PATH}}"
        eval "_type=\${${TYP_BASE}${VAR_DELIM}${CUR_PATH}}";
        _isNew=;

        VAL=${2};

        if [ -n "${UPP_PATH}" -a -z "${_p_type}" ] ; then
            echo "E: Unknown parent type for field '${str}'" >&2;
            return;
        elif [ -z "${_type}" -a -z "${3}" ] ; then
            echo "E: Unknown type of new field '${str}'" >&2;
            return;
        elif [ -n "${_type}" -a -z "${3}" ] ; then
            #echo "E: Clearing type of field '${str}'" >&2;
            #return;
            #_type="${3}";
            :;
        elif [ -n "${_type}" -a -n "${3}" -a "${_type}" != "${3}" ] ; then
            echo "W: Changing type of field '${str}' from '${_type}' to '${3}'" >&2;
            _type="${3}";
        elif [ -z "${_type}" -a -n "${3}" ] ; then
            _isNew=1;
            _type="${3}";
        elif [ -n "${_type}" -a -n "${3}" ] ; then
            _type="${3}";
        elif [ -n "${_type}" -a -z "${3}" ] ; then
            :;
        else
            echo "E: Unknown state" >&2;
            return;
        fi;
        
        
        
        eval "${KEY_BASE}${VAR_DELIM}${CUR_PATH}=${VAL}";
        eval "${TYP_BASE}${VAR_DELIM}${CUR_PATH}=${_type}";

        if isSet ${_isNew} ; then
            eval "${UPP_BASE}${VAR_DELIM}${CUR_PATH}=${UPP_PATH}";
#            eval "${IDX_BASE}${VAR_DELIM}${CUR_PATH}=";
            eval "${MMB_BASE}${VAR_DELIM}${UPP_PATH}=\"\${${MMB_BASE}${VAR_DELIM}${UPP_PATH}}${MMB_DELIM}${KEY}\"";
            eval "${LEN_BASE}${VAR_DELIM}${UPP_PATH}=\$((${LEN_BASE}${VAR_DELIM}${UPP_PATH}+1))";
        fi;
    ## }}}


    elif [ "${MODE}" = "compile" ] ; then
        CUR_ITEM=;
        CUR_PATH=;
        UPP_PATH=;
        str=;
        for _prepend in {1} ; do
            CUR_PATH="${CUR_PATH}${VAR_DELIM}";
        done;
        
        local _items=;
        
        ## This is dangerous. TODO make some safety switches for the future
        while :; do
            
            NEW_BLOCK=;
            KEY=;
            
            eval "_p_type=\${${TYP_BASE}${VAR_DELIM}${UPP_PATH}}"
            eval "_type=\${${TYP_BASE}${VAR_DELIM}${CUR_PATH}}";

            eval "local CUR_ITEM=\${CUR_ITEM_${CUR_PATH}}";
            
            if [ "${_type}" = "object" ] ; then
                NEW_BLOCK=object;
                if ! isSet "${CUR_ITEM}" ; then
                    str="${str}{";
                fi;
            elif [ "${_type}" = "array" ] ; then
                NEW_BLOCK=array;
                if ! isSet "${CUR_ITEM}" ; then
                    str="${str}[";
                fi;
            fi;
            
            if isSet ${NEW_BLOCK} ; then
                eval "_items=\${${MMB_BASE}${VAR_DELIM}${CUR_PATH}}";
                _items=${_items/${MMB_DELIM}/}
                local _hasMoreItems=;
                local _nextItem=;
                local _fforwardOk=;
                local _prevItem=;
                
                if isSet "${CUR_ITEM}" ; then
                    _prevItem=1;
                fi;
                
                while read _item ; do
                    ___item="_______________${_item}" ## debug
                    if isSet "${CUR_ITEM}" && ! isSet ${_fforwardOk} ; then
                        if [ "${_item}" != "${CUR_ITEM}" ] ; then
                            continue;
                        else
                            _fforwardOk=1;
                            continue;
                        fi;
                    fi;
                    
                    if isSet ${_nextItem} ; then
                        _hasMoreItems=1;
                        break;
                    else
                        _nextItem=1;
                        KEY=${_item};
                        eval "local CUR_ITEM_${CUR_PATH}=${_item}"
                        eval "UPP_PATH=${CUR_PATH}";
                        eval "CUR_PATH=${CUR_PATH}${VAR_DELIM}${_item}";
                        #eval "${UPP_BASE}${VAR_DELIM}${CUR_PATH}=${UPP_PATH}"
                        continue;
                    fi;
                    
                done <<< "${_items//${MMB_DELIM}/
}"
                
                if isSet ${_nextItem} ; then
                    if isSet ${_prevItem} ; then
                        str="${str},";
                    fi;
                
                    if [ "${_type}" = "object" ] ; then
                        str="${str}\"${KEY}\":"
                    else
                        :;
                    fi;

                    continue;
                fi;

                case ${_type} in
                    object) str="${str}}"  ;;
                    array)  str="${str}]"  ;;
                    *) echo "Unknown data type: '${_type}'" >&2 ;;
                esac;
                eval "CUR_PATH=${UPP_PATH}";
                eval "UPP_PATH=\${${UPP_BASE}${VAR_DELIM}${CUR_PATH}}";
            else
                eval "VAL=\${${KEY_BASE}${VAR_DELIM}${CUR_PATH}}";

                case ${_type} in
                    string)  str="${str}\"${VAL}\""  ;;
                    literal) str="${str}${VAL}"      ;;
                    *) echo "Unknown data type: '${_type}'" >&2 ;;
                esac;
                
                eval "CUR_PATH=${UPP_PATH}";
                eval "UPP_PATH=\${${UPP_BASE}${VAR_DELIM}${CUR_PATH}}";
            fi;
                
            if ! isSet "${CUR_PATH}" ; then
                break;
            fi;
            :;
        done;


        echo "${str}";
    fi;

    ## }}}

}

## }}}


## {{{ Shortcut functions
bjson_parse() {
    parseJson "${1:?JSON string not given}"
}

bjson_find() {
    BJMODE=find parseJson "${1:?JSON path not given for lookup}"
}

bjson_write() {
    local path=${1:?JSON value path not given}
    local value=${2:?Value to write not given}
    local type=${3:?Value type not given}

    BJMODE=write parseJson "${path}" "${value}" "${type}"
}

bjson_compile() {
    BJMODE=compile parseJson
}

bjson_clear() {
    clearParsed
}

## }}}


## {{{ autocomplete

_prefix_all() {
    local prefix=${1:?Unknown prefix}
    for str in ${2} ; do
        printf "${prefix}${str} "
    done
}

_filter_prefixed() {
    local prefix=${1}
    local items=${2}

    if [ -z "${prefix}" ] ; then
        printf "${items}"
        return 0
    fi;

    for item in ${items} ; do
        egrep -q "^${prefix}" <<< "${item}";
        if [[ ${?} -eq 0 ]] ; then
            printf "${item} "
        fi;
    done;
}

_bjson_find() {
    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"

    if [ -z "${cur}" ] ; then
        COMPREPLY=
        return 0;
    fi;

    local path_cur=${cur%.*}
    local path_last=${cur##*.}

    local members=$(BJMODE=find parseJson "${path_cur}");
    members="${members//,/}"
    members=$(_filter_prefixed "${path_last}" "${members}")
    members=$(_prefix_all "${path_cur}." "${members}")
#echo "members=${members}" >&2
    COMPREPLY=(${members})
    return 0;
} && complete -F _bjson_find bjson_find


## }}}

